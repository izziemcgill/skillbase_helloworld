from django.shortcuts import render_to_response, redirect, get_list_or_404, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt

from django.core.context_processors import csrf
from django.forms.models import modelformset_factory
from django.template import RequestContext
from django.core.urlresolvers import reverse

import time
import calendar
from datetime import date, datetime, timedelta

from models import Role, Employee, Course, Event
        
mnames = "January February March April May June July August September October November December".split() 
    
@login_required
def home(request):
    
    roles, courses = [], []
    manager = request.GET.get('code') or '60305'

    Manager = Employee.objects.filter(code=manager)[0]
    Employees = Manager.get_minions(include_self=False) or [ Manager ]
    for employee in Employees:
        roles += employee.roles
    roles = sorted(set(roles))
    
    Roles = Role.objects.all().filter(code__in=roles)    
    for role in Roles:
        courses += role.courses    
    courses = sorted(set(courses))
    
    Courses = Course.objects.all().filter(code__in=courses)
    Roles = Role.objects.all().filter(code__in=roles)
     
    parameters = {'Manager': Manager, 'Employees': Employees, 'Courses': Courses, 'Roles': Roles }
    
    if Manager.headcount:
        return render_to_response('world/grid.html', parameters)
    else:
        return render_to_response('world/employee.html', parameters)
    
@login_required    
def menu(request, course_id, employee_id):
    
    employee = Employee.objects.get(id=employee_id)
    course = Course.objects.get(id=course_id)
    
    action = request.GET.get('action') 
    if action == 'cancel':
        employee.cancelled = employee.cancelled + [course.code]
    elif action == 'reject':
        employee.rejected = employee.rejected + [course.code]
    elif action == 'approve':
        employee.approved = employee.approved + [course.code]
    elif action == 'book':
        employee.booked = employee.booked + [course.code]
    elif action == 'add':
        employee.courses = employee.courses + [course.code]
    elif action == 'undo':
        try:
            status = request.GET.get('status') 
            if status == 'completed':
                employee.completed.remove(course.code)
            elif status == 'approved':
                employee.approved.remove(course.code)
            elif status == 'rejected':
                employee.rejected.remove(course.code)
            elif status == 'booked':
                employee.booked.remove(course.code)
            elif status == 'cancelled':
                employee.cancelled.remove(course.code)
            elif status == 'failed':
                employee.failed.remove(course.code)
            elif status == 'absent':
                employee.attended = employee.attended + [course.code]
            elif status == 'add':
                employee.courses.remove(course.code)
            else:
                pass 
        except IndexError, e:
            print "There was no such item in the list"
    elif action == 'comment':
        pass
    else:
        pass
    
    employee.save()
    
    return redirect(request.META['HTTP_REFERER'])

@login_required
def role(request, roleid):

    role = Role.objects.get(id=roleid)

    Roles = Role.objects.all().exclude(courses__isnull=True).exclude(courses__exact='').order_by('name')
    Courses = Course.objects.all().filter(code__in=role.courses)
    Employees = Employee.objects.all().filter(roles__contains=role.code)

    return render_to_response('world/matrices.html', {'role': role, 'Roles': Roles, 'Courses': Courses, 'Employees': Employees })

@login_required
def dash(request):
    
    for course in courses:
        employees = Employee.objects.all().filter(attended__contains=course.code)
        if employees.count() > 0:
            d.update({course.name : employees.count()})
        else:
            unused.append(course.id)
        
    top10 = sorted(d.items(), key=lambda x: x[1], reverse=True)[10]
    
    return render_to_response('world/dash.html', {'Popular': top10, 'back': request.META['HTTP_REFERER'] })

def _show_users(request):
    """Return show_users setting; if it does not exist, initialize it."""
    s = request.session
    if not "show_users" in s:
        s["show_users"] = True
    return s["show_users"]

@login_required
def settings(request):
    """Settings screen."""
    s = request.session
    _show_users(request)
    if request.method == "POST":
        s["show_users"] = (True if "show_users" in request.POST else False)
    return render_to_response("world/settings.html", add_csrf(request, show_users=s["show_users"]))

def reminders(request):
    """Return the list of reminders for today and tomorrow."""
    year, month, day = time.localtime()[:3]
    end_date = datetime(year=year, month=month, day=day) + timedelta(1)
    start_date = end_date - timedelta(7)
    reminders = Event.objects.filter(date__range=(start_date, end_date)).filter(creator=request.user).filter(remind=True)
    return list(reminders) 

@login_required
def year(request, year=None):
    """Main listing, years and months; three years per page."""
    # prev / next years
    if year: 
        year = int(year)
    else:
        year = time.localtime()[0]

    nowy, nowm = time.localtime()[:2]
    lst = []

    # create a list of months for each year, indicating ones that contain entries and current
    for y in [year, year+1, year+2]:
        mlst = []
        for n, month in enumerate(mnames):
            entry = current = False   # are there entry(s) for this month; current month?

            #mongo no date queries
            start_date = datetime(year=y, month=n+1, day=1)
            end_date = start_date + timedelta(calendar.monthrange(year, n+1)[1]) + timedelta(-1)
            events = Event.objects.filter(date__range=(start_date, end_date))
            #events = Event.objects.filter(date__year=y, date__month=n+1)
            
            if not _show_users(request):
                events = events.filter(creator=request.user)

            if events:
                entry = True
            if y == nowy and n+1 == nowm:
                current = True
            mlst.append(dict(n=n+1, name=month, entry=entry, current=current))
        lst.append((y, mlst))

    return render_to_response("world/year.html", dict(years=lst, user=request.user, year=year,
                                                   reminders=reminders(request)))

@login_required
def month(request, year, month, change=None):
    """Listing of days in `month`."""
    year, month = int(year), int(month)

    # apply next / previous change
    if change in ("next", "prev"):
        now, mdelta = date(year, month, 15), timedelta(days=31)
        if change == "next":   mod = mdelta
        elif change == "prev": mod = -mdelta

        year, month = (now+mod).timetuple()[:2]

    # init variables
    cal = calendar.Calendar()
    month_days = cal.itermonthdays(year, month)
    nyear, nmonth, nday = time.localtime()[:3]
    lst = [[]]
    week = 0

    # make month lists containing list of days for each week
    # each day tuple will contain list of events and 'current' indicator
    for day in month_days:
        events = current = False   # are there events for this day; current day?
        if day:
            day_date = datetime(year=int(year), month=int(month), day=int(day))
            events = Event.objects.filter(date=day_date)
            if not _show_users(request):
                events = events.filter(creator=request.user)
            if day == nday and year == nyear and month == nmonth:
                current = True

        lst[week].append((day, events, current))
        if len(lst[week]) == 7:
            lst.append([])
            week += 1

    return render_to_response("world/month.html", dict(year=year, month=month, user=request.user,
                        month_days=lst, mname=mnames[month-1], reminders=reminders(request)))

@login_required
def day(request, year, month, day):
    
    day_date = datetime(year=int(year), month=int(month), day=int(day))
    
    """Entries for the day."""
    EntriesFormset = modelformset_factory(Event, extra=0, exclude=("creator", "date"), can_delete=True)
    
    other_events = []
    if _show_users(request):
        other_events = Event.objects.filter(date=day_date).exclude(creator=request.user)

    if request.method == 'POST':
        formset = EntriesFormset(request.POST)
        if formset.is_valid():
            # add current user and date to each entry & save
            events = formset.save(commit=False)
            for entry in events:
                entry.creator = request.user
                entry.date = date(int(year), int(month), int(day))
                entry.save()
            return HttpResponseRedirect(reverse("world.views.month", args=(year, month)))

    else:# display formset for existing enties and one extra form
        formset = EntriesFormset(queryset=Event.objects.filter(date=day_date, creator=request.user))
    
    return render_to_response("world/day.html", add_csrf(request, events=formset, year=year,
            month=month, day=day, other_events=other_events, reminders=reminders(request)))


def add_csrf(request, **kwargs):
    """Add CSRF and user to dictionary."""
    d = dict(user=request.user, **kwargs)
    d.update(csrf(request))
    return d
