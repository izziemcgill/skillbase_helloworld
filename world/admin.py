from django.contrib import admin
from world.models import Role, Employee, Course, Event, Provider

ITEMS_PER_PAGE = 25

def finish(modeladmin, request, queryset):
    #queryset.update(status='F') # dummy update to fire trigger
    pass
finish.short_description = "Finish"

class RoleAdmin(admin.ModelAdmin):
    list_per_page = ITEMS_PER_PAGE
    search_fields = ('name', 'code', 'courses',)

class EmployeeAdmin(admin.ModelAdmin):
    list_per_page = ITEMS_PER_PAGE
    list_display = ('name', 'login', 'jobtitle', 'division', 'location', 'manager', 'staff', )
    search_fields = ('code', 'name', 'courses',)

    actions = [finish]
    
    def staff(self, obj):
        return '<a href="/?code=%s">%s</a>' % (obj.code, obj.headcount)
    staff.allow_tags = True
    
    fieldsets = (
        (None, {
            'fields': ('code', 'name', 'login', 'jobtitle', 'division', 'location', 'manager', )
        }),
        ('Other', {
            'classes': ('collapse',),
            'fields': ( 'courses', 'approved', 'rejected', 'booked', 'attended', 'failed', 'cancelled', 'completed', 'roles',)
        }),
    )
    
class CourseAdmin(admin.ModelAdmin):
    list_per_page = ITEMS_PER_PAGE
    list_display = ('name', 'category', 'provider', 'cost',) 
    search_fields = ('code', 'name', 'category', 'provider', )

class EventAdmin(admin.ModelAdmin):
    list_display = ["creator", "date", "title", "booked"]
    search_fields = ["title", "body"]
    list_filter = ["creator"]

class ProviderAdmin(admin.ModelAdmin):
    list_per_page = ITEMS_PER_PAGE
    list_display = ('name', 'contact',) 
    search_fields = ('name', 'courses', 'special', 'contact', 'account',)

admin.site.register(Role, RoleAdmin)
admin.site.register(Employee, EmployeeAdmin)
admin.site.register(Course, CourseAdmin)
admin.site.register(Event, EventAdmin)
admin.site.register(Provider, ProviderAdmin)
