from django.contrib.auth.models import User
from django.db import models
from django import forms

from djangotoolbox.fields import ListField
from pymongo import MongoClient

class StringListField(forms.CharField):
    def prepare_value(self, value):
        if value:
            return ', '.join(value)
        else:
            return ''

    def to_python(self, value):
        print "to_python %s" % value
        if not value:
            return []
        return [item.strip() for item in value.split(',')]

class MultiField(ListField):
    def formfield(self, **kwargs):
        return models.Field.formfield(self, StringListField, **kwargs)

class Role(models.Model):  
    code = models.CharField(max_length=32)
    name = models.CharField(max_length=144) 
    courses = MultiField(null=True, blank=True)

    def __unicode__(self):
        return u'%s' % (self.name)

class Course(models.Model):  
    code = models.CharField(max_length=32)
    name = models.CharField(max_length=144) 
    category = models.CharField(max_length=144) 
    provider = models.CharField(max_length=144) 
    contacts = models.CharField(max_length=10000) 
    cost = models.DecimalField(null=True, blank=True, max_digits=9, decimal_places=2)

    def __unicode__(self):
        return u'%s' % (self.name)

class Employee(models.Model):                                                                                                                                  
    code = models.IntegerField()
    name = models.CharField(max_length=144)
    login = models.CharField(max_length=64)
    jobtitle = models.CharField(max_length=144)
    division = models.CharField(max_length=144)
    location = models.CharField(max_length=144)
    manager = models.ForeignKey('self', null=True, blank=True)
    courses = MultiField(null=True, blank=True)
    approved = MultiField(null=True, blank=True)
    rejected = MultiField(null=True, blank=True)
    booked = MultiField(null=True, blank=True)
    attended = MultiField(null=True, blank=True)
    failed = MultiField(null=True, blank=True)
    cancelled = MultiField(null=True, blank=True)
    completed = MultiField(null=True, blank=True)
    roles = MultiField(null=True, blank=True)

    def get_minions(self, include_self=True):
        r = []
        if include_self:
            r += [self]
        for c in Employee.objects.filter(manager=self.id):
            r += c.get_minions(include_self=True)
        return r
    
    # count minions
    def _headcount(self): return Employee.objects.all().filter(manager=self.id).count()
    headcount = property(_headcount)

    def __unicode__(self):
        return u'%s' % (self.name)
    
class Event(models.Model):
    title = models.CharField(max_length=40)
    body = models.TextField(max_length=10000, blank=True)
    #parent = models.ForeignKey('self', null=True, blank=True) 
    created = models.DateTimeField(auto_now_add=True)
    creator = models.ForeignKey(User, blank=True, null=True)
    date = models.DateField(blank=True)
    remind = models.BooleanField(default=False)
    booked = MultiField(null=True, blank=True)
    attended = MultiField(null=True, blank=True)
    
    def __unicode__(self):
        return u'%s' % (self.title)

class Provider(models.Model):
    name = models.CharField(max_length=40)
    account = models.CharField(max_length=144)
    contact = models.CharField(max_length=144)
    courses = MultiField(null=True, blank=True)
    special = MultiField(null=True, blank=True)
    
    def __unicode__(self):
        return u'%s' % (self.name)
