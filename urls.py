from django.conf.urls.defaults import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^course/(.*)/employee/(.*)/$', 'world.views.menu'),
    url(r'^role/(.*)/$', 'world.views.role'),
    url(r'^month/(\d+)/(\d+)/(prev|next)/$', 'world.views.month'),
    url(r'^month/(\d+)/(\d+)/$', 'world.views.month'),
    url(r'^month$', 'world.views.month'),
    url(r'^day/(\d+)/(\d+)/(\d+)/$', 'world.views.day'),
    url(r'^settings/$', 'world.views.settings'),
    url(r'^year/(\d+)/$', 'world.views.year'),
    url(r'^year$', 'world.views.year'),
    url(r'^$', 'world.views.home'),
)

